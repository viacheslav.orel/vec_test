﻿using System;
using System.Linq;
using System.Windows.Forms;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using CreateLevelsFromExcel.Models;
using Ganss.Excel;

namespace CreateLevelsFromExcel.Addins
{
    [Transaction(TransactionMode.Manual)]
    public class GetLevelsInfo : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            var doc = commandData.Application.ActiveUIDocument.Document;
            
            // Get date from excel
            var openFileDialog = new OpenFileDialog
            {
                Filter = "Table file (*.xlsx)|*.xlsx",
                RestoreDirectory = true,
                Title = "Select file with levels info"
            };

            if (openFileDialog.ShowDialog() != DialogResult.OK) return Result.Cancelled;
            
            var levelsInTable = new ExcelMapper(openFileDialog.FileName).Fetch<LevelsData>().ToArray();
            
            // Get levels in model
            var levelsInModel = new FilteredElementCollector(doc)
                .OfCategory(BuiltInCategory.OST_Levels)
                .WhereElementIsNotElementType();
            
            // Change existing level
            using (var tr = new Transaction(doc, "Change existing level"))
            {
                tr.Start();

                foreach (var levelInModel in levelsInModel)
                {
                    var elevation = Math.Abs(UnitUtils.ConvertFromInternalUnits(
                        levelInModel.get_Parameter(BuiltInParameter.LEVEL_ELEV).AsDouble(), DisplayUnitType.DUT_FEET_FRACTIONAL_INCHES));
                    var levelDataIndex = Array.FindIndex(levelsInTable,
                        data => Math.Abs(data.Elevation - elevation) < 0.01);

                    if (levelDataIndex == -1)
                    {
                        doc.Delete(levelInModel.Id);
                    }
                    else
                    {
                        levelInModel.get_Parameter(BuiltInParameter.LEVEL_ELEV).Set(
                            UnitUtils.ConvertToInternalUnits(levelsInTable[levelDataIndex].Elevation,
                                DisplayUnitType.DUT_FEET_FRACTIONAL_INCHES));
                        levelInModel.Name = levelsInTable[levelDataIndex].Name;

                        levelsInTable[levelDataIndex].IsExist = true;
                    }
                }
                
                tr.Commit();
            }
            
            // Add new levels
            using (var tr = new Transaction(doc, "Create new levels"))
            {
                tr.Start();

                foreach (var data in levelsInTable)
                {
                    if (data.IsExist) continue;

                    var level = Level.Create(doc,
                        UnitUtils.ConvertToInternalUnits(data.Elevation, DisplayUnitType.DUT_FEET_FRACTIONAL_INCHES));
                    level.Name = data.Name;
                }

                tr.Commit();
            }

            MessageBox.Show("Well Done!");
            
            return Result.Succeeded;
        }
    }
}