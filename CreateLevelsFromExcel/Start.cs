﻿using Autodesk.Revit.UI;

namespace CreateLevelsFromExcel
{
    public class Start : IExternalApplication
    {
        public Result OnStartup(UIControlledApplication application)
        {
            const string PATH = @"C:\ProgramData\Autodesk\Revit\Addins\2020\VEC_test\";
            // Create ribbon tab
            const string TAB_NAME = "VEC_Test";
            application.CreateRibbonTab(TAB_NAME);
            
            // Create ribbon panel
            var basePanel = application.CreateRibbonPanel(TAB_NAME, "Base");
            
            
            // Create Add level button
            var addLevelsData = new PushButtonData(
                "addLevels",
                "Add Levels",
                $"{PATH}CreateLevelsFromExcel.dll",
                "CreateLevelsFromExcel.Addins.GetLevelsInfo");

            basePanel.AddItem(addLevelsData);
            return Result.Succeeded;
        }

        public Result OnShutdown(UIControlledApplication application)
        {
            return Result.Succeeded;
        }
    }
}