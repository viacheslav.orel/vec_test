﻿using Ganss.Excel;
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace CreateLevelsFromExcel.Models
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class LevelsData
    {
        public string Name { get; set; }
        [Column("Elevation Z")]
        public double Elevation { get; set; }

        public bool IsExist { get; set; } = false;
    }
}